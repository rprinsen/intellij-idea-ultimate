-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: intellij-idea-ultimate
Binary: intellij-idea-ultimate
Architecture: all
Version: 2016.3.4-1
Maintainer: Marcel Michael Kapfer <marcelmichaelkapfer@yahoo.co.nz>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 7.0.50~)
Package-List:
 intellij-idea-ultimate deb devel optional arch=all
Checksums-Sha1:
 e44d4de41689e38b8d817442e2bae79b6c69c5e9 5674 intellij-idea-ultimate_2016.3.4.orig.tar.gz
 43835eeb1edc1d1295f04ac20112a23a77619058 9156 intellij-idea-ultimate_2016.3.4-1.debian.tar.xz
Checksums-Sha256:
 288e100b8868689892055bc4db32a47e18c4e1c2066dc454609f6697f569a1ff 5674 intellij-idea-ultimate_2016.3.4.orig.tar.gz
 6db1be601a94137fb38a37e8ce2c9b1411922b61356f044029d29dc8675d03bd 9156 intellij-idea-ultimate_2016.3.4-1.debian.tar.xz
Files:
 cc13278c6f1ce3f252344f504119a3f5 5674 intellij-idea-ultimate_2016.3.4.orig.tar.gz
 dad82c4bdafbe78522199939f92396f9 9156 intellij-idea-ultimate_2016.3.4-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQI9BAEBCAAnBQJYnd8jIBxtYXJjZWxtaWNoYWVsa2FwZmVyQHlhaG9vLmNvLm56
AAoJECuF2UqwCcGYxFMP/Ankuqjyn+V/rNJz9Ze9tDLk6HAyFOpdBvR4PFmhuZVv
W46S6oKpQi1NJwUZrCJFHpmGIUui3xr3v94fMYViG9eM+SNGkBRgS2Ciy+YaMpWQ
VkKl5cCoX2SqoDu2Kn6dIRhf8Adcb/oWIQxANll5bbkkeesZFMxJTq7c4ra8PfZV
8kjLXesiJXBz6dMf+Qn6od6cwxuo4Qx9q/VhBsIIqpWO+SRsuY5g6T/7fKPBpHxU
XWbsa6CciXw70nQHrHl3iXjE6KAZydvF2zXOoNOzOs6/oPSQWSmIdFSZasYDihbW
i+qxVNEjMLBfFGo8K2fFaCH66DPlHa2huMrUsHKuZasAHT/c9H5x9ENSON3CsqZ8
u8Det3Mrv0AnLGImhkOJ87ZepIvpR6pxHUIuoMJLyVwRRCyyVbM6Scpvsa4U6BA1
MJwAkPYpMpEDgCNUHP4BlVd9Lc5aIlPXe0N5QYd4Pgnj2gwOCNSZzSNv3tXe23h+
qR8igSzpwK3fFc4XpSRNrySMw7cuoYQOBKNljBjw0lVVTgwR1EDMtGVWjFysSxjN
Kx6i7Rg5lS0hPKI4lv2khyGPrbr34bReCq8Qrv3vFsOF6vVDQXffz8/g5/NMrYJu
qdD6qdgnZbt9hYkE8H9SVnuZU0KIxm+uy/4+pBeqITk809djdnXk+YPLUK75Lznp
=5Rr+
-----END PGP SIGNATURE-----
